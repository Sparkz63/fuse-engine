#include "SampleGame.h"
#include <iostream>


SampleGame::SampleGame()
{
}


SampleGame::~SampleGame()
{
}


/*
 initialize() will be called before the first game loop. 
 For this sample game, here we will load all the mesh
 data and initialize all variables.
*/
void SampleGame::initialize()
{
	// Contain mouse within window.
	SDL_SetRelativeMouseMode(SDL_TRUE);

	// Print OpenGL Version
	std::cout << glGetString(GL_VERSION) << std::endl;

	printf("Initializing SampleGame...\n");
	WINDOW_TITLE = "FUSE Sample Game";

	// Initialize player
	player = new Player(glm::vec3(0, 0, -1), glm::vec3(0, 0, 1));
	player->initialze();

	// Initialize camera
	camera = new Camera(glm::vec3(0, 0, -1), 70.0f, (float)SCREEN_WIDTH / (float)SCREEN_HEIGHT, 1.5f, 1000.f);
	camera->setForwardRotation(glm::vec3(0, 0, 1));

	// Load in mesh from .obj
	printf("Building Mesh (this may take a while)...\n");

	//mesh = new Mesh("./res/serpentine city.obj");
	//mesh = new Mesh("./res/Organodron City.obj");
	mesh = new Mesh("./res/Kameri lunar colony.obj");
	
	// Describe a transform for our mesh
	transform = new Transform();
	transform->setScale(glm::vec3(.2, .2, .2));

	// Compile shader
	printf("Compiling Shaders...\n");
	shader = new Shader("./res/basicShader");

	// Load sky box
	printf("Building Sky Box...\n");
	skyBox = new SkyBox(
		"./res/",
		"sky_front.jpg",
		"sky_back.jpg",
		"sky_left.jpg",
		"sky_right.jpg",
		"sky_top.jpg",
		"sky_bottom.jpg"
		);

	// Finished loading!
	printf("Done. Press ESC to quit.");
}

/*
 update() will be called during each game loop, directly before
 rendering the scene. During this call we will set positions, handle
 input, and update the window header.

 parameters:
 Uint32 deltaTime - the number of milliseconds since the last frame.
 std::vector<SDL_Event> eventVector - contains all unprocessed input events.
*/
void SampleGame::update(Uint32 deltaTime, std::vector<SDL_Event> eventVector)
{
	// Quit application if escape key is pressed.
	for each(SDL_Event e in eventVector) {
		if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_ESCAPE)
			quitApplication = true;
	}

	// Set player position & camera orientation
	player->update(deltaTime, eventVector);
	camera->setPosition(player->getPosition());
	camera->setForwardRotation(player->getForward());

	// Update window header with some extra information
	frame++;
	std::string s = std::string(WINDOW_TITLE) + " - Update # " + std::to_string(frame) + ";  deltaTime = " + std::to_string(deltaTime) + ";  fps = " + std::to_string(fps);
	SDL_SetWindowTitle(window, s.c_str());
}

/*
 render() will be called after updating during each game loop.
 During this call, we will render our skybox and mesh.
*/
void SampleGame::render()
{
	// Render skybox
	skyBox->render(*camera);

	// Bind shader to drawing context with given camera and transform
	shader->bind();
	shader->update(*transform, *camera);

	// Draw mesh to screen
	mesh->draw();
}

/*
 Free memory once game is finished
*/
void SampleGame::deinitialize() 
{
	free(mesh);
	free(shader);
	free(vertices);
	free(camera);
	free(transform);
	free(player);
	free(skyBox);
}