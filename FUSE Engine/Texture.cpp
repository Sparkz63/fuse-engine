#include "Texture.h"

Texture::Texture(const std::string &filename)
{
	int width, height, numComponents;
	unsigned char *imageData = stbi_load(filename.c_str(), &width, &height, &numComponents, 4);

	if (imageData == NULL)
		std::cerr << "imageData = null!\n";

	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, imageData);

	glGenerateMipmap(GL_TEXTURE_2D);

	glGenSamplers(1, &sampler);

	stbi_image_free(imageData);
}


Texture::~Texture()
{
	glDeleteTextures(1, &texture);
}

GLuint Texture::getTexId() {
	return texture;
}

GLuint Texture::getSampler()
{
	return sampler;
}

void Texture::bind(unsigned int unit) 
{
	assert(unit >= 0 && unit <= 31);
	glActiveTexture(GL_TEXTURE0 + unit);
	glBindTexture(GL_TEXTURE_2D, texture);
}