#include "Mesh.h"
#include <fstream>

Mesh::Mesh(const std::string& filename)
{
	std::vector<tinyobj::shape_t> shapes;
	std::string err = tinyobj::LoadObj(shapes, filename.c_str(), ".//res//");

	if (!err.empty()) 
	{
		std::cout << err << std::endl;
		system("pause");
		exit(1);
	}

	std::cout << "shapes: " << shapes.size() << std::endl;

	initMesh(shapes);
}

Mesh::~Mesh()
{
	for each(VertexArray v in arrays)
		glDeleteVertexArrays(1, &v.vertexArrayObject);

	for each (Texture* t in textures)
		free(t);
}

void Mesh::initMesh(const std::vector<tinyobj::shape_t> shapes)
{
	unsigned int numShapes = shapes.size();
	textures.resize(numShapes);
	
	for (int i = 0; i < numShapes; i++)
	{
		VertexArray v;
		v.drawCount = shapes[i].mesh.indices.size();
		glGenVertexArrays(1, &v.vertexArrayObject);
		glGenBuffers(NUM_BUFFERS, v.vertexArrayBuffers);
		this->arrays.push_back(v);
	}

	for (int i = 0; i < arrays.size(); i++)
	{
		std::vector <glm::vec3> positions;
		std::vector <glm::vec2> texCoords;
		std::vector <glm::vec3> normals;
		std::vector <unsigned int> indices;

		// Get positions
		for (int v = 0; v < shapes[i].mesh.positions.size() / 3; v++) 
		{
			positions.push_back(glm::vec3(
				shapes[i].mesh.positions[3 * v + 0],
				shapes[i].mesh.positions[3 * v + 1],
				shapes[i].mesh.positions[3 * v + 2]
				));
		}

		// Get texture coordinates
		for (int v = 0; v < shapes[i].mesh.texcoords.size() / 2; v++)
		{
			texCoords.push_back(glm::vec2(
				shapes[i].mesh.texcoords[2 * v + 0],
				shapes[i].mesh.texcoords[2 * v + 1]
				));
		}

		// Get normals
		for (int v = 0; v < shapes[i].mesh.normals.size() / 3; v++)
		{
			normals.push_back(glm::vec3(
				shapes[i].mesh.normals[3 * v + 0],
				shapes[i].mesh.normals[3 * v + 1],
				shapes[i].mesh.normals[3 * v + 2]
				));
		}

		// Get indices
		for (int v = 0; v < shapes[i].mesh.indices.size(); v++)
		{
			indices.push_back(shapes[i].mesh.indices[v]);
		}

		// Get texture
		if (shapes[i].material.diffuse_texname != "") {
			std::string path = "./res" + shapes[i].material.diffuse_texname;
			std::cout << "texname[" << i << "] " << path << std::endl;
			textures[i] = new Texture(path);
		}
		else {
			textures[i] = NULL;
		}

		// Write to GPU
		glBindVertexArray(arrays[i].vertexArrayObject);

		glBindBuffer(GL_ARRAY_BUFFER, arrays[i].vertexArrayBuffers[POSITION_VB]);
		glBufferData(GL_ARRAY_BUFFER, positions.size() * sizeof(positions[0]), &positions[0], GL_STATIC_DRAW);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

		glBindBuffer(GL_ARRAY_BUFFER, arrays[i].vertexArrayBuffers[TEXCOORD_VB]);
		glBufferData(GL_ARRAY_BUFFER, positions.size() * sizeof(texCoords[0]), &texCoords[0], GL_STATIC_DRAW);

		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);

		if (normals.size() > 0)
		{
			glBindBuffer(GL_ARRAY_BUFFER, arrays[i].vertexArrayBuffers[NORMAL_VB]);
			glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(normals[0]), &normals[0], GL_STATIC_DRAW);

			glEnableVertexAttribArray(2);
			glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, 0);
		}

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, arrays[i].vertexArrayBuffers[INDEX_VB]);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(indices[0]), &indices[0], GL_STATIC_DRAW);

		glBindVertexArray(0);
	}
}

/*
 Draw mesh using gl
*/
void Mesh::draw()
{
	for (int i = 0; i < arrays.size(); i++)
	{
		if (textures[i] != NULL)
			textures[i]->bind(0);

		glBindVertexArray(arrays[i].vertexArrayObject);
		glDrawElements(GL_TRIANGLES, arrays[i].drawCount, GL_UNSIGNED_INT, 0);

		glBindVertexArray(0);
	}
}
