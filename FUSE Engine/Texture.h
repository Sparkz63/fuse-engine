#pragma once

#include <glew.h>
#include <string>
#include "stb_image.h"
#include <cassert>
#include <iostream>

class Texture
{
public:
	Texture(const std::string &filename);

	GLuint getSampler();
	GLuint getTexId();

	void bind(unsigned int unit);

	~Texture();

private:
	GLuint texture;
	GLuint sampler;
};

