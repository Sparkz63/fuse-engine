#include "Player.h"


Player::Player(glm::vec3 position, glm::vec3 forward)
{
	this->position = position;
	this->forward = forward;
}

void Player::initialze() 
{

	for (int n = 0; n < NUM_CONTROLS; n++)
	{
		controller[n] = false;
	}
}

void Player::update(Uint32 deltaTime, std::vector<SDL_Event> eventVector)
{
	handleEvents(eventVector);

	float moveAmount = (float)deltaTime * moveSpeed / 1000.f;

	if (controller[MOVE_FORWARD])
	{
		position.z += forward.z * moveAmount;
		position.x += forward.x * moveAmount;
	}
	if (controller[MOVE_BACKWARD])
	{
		position.z -= forward.z * moveAmount;
		position.x -= forward.x * moveAmount;
	}
	if (controller[MOVE_LEFT])
	{
		glm::vec3 left = glm::rotateY(forward, 90.f);
		position.z += left.z * moveAmount;
		position.x += left.x * moveAmount;
	}
	if (controller[MOVE_RIGHT])
	{
		glm::vec3 left = glm::rotateY(forward, 90.f);
		position.z -= left.z * moveAmount;
		position.x -= left.x * moveAmount;
	}
	if (controller[MOVE_UP])
	{
		position.y += moveAmount;
	}
	if (controller[MOVE_DOWN])
	{
		position.y -= moveAmount;
	}


	float turnAmountX = motion.x * turnSpeed;
	float turnAmountY = motion.y * turnSpeed;
	if (abs(motion.x) > turnThreshold)
		yawRotation += turnAmountX;
	if (abs(motion.y) > turnThreshold)
		pitchRotation -= turnAmountY;

	motion = glm::vec2(0, 0);

	
	if (yawRotation > 360) {
		yawRotation -= 360;
	}
	else if (yawRotation < 0) {
		yawRotation += 360;
	}

	if (pitchRotation > 70)
		pitchRotation = 70;
	else if (pitchRotation < -70)
		pitchRotation = -70;

	forward = glm::rotateX(glm::vec3(0, 0, 1), -pitchRotation);
	forward = glm::rotateY(forward, -yawRotation);

	//std::cout << "yaw: \t" << yawRotation << "; (\t" << forward.x << ", \t" << forward.z << ") " << std::endl;
	//std::cout << motion.x << ", " <<  motion.y << "\tpitch: \t" << pitchRotation << ", yaw: \t" << yawRotation << std::endl;
}

void Player::handleEvents(std::vector<SDL_Event> eventVector) {
	for each(SDL_Event curEvent in eventVector)
	{
		// handle key down events.
		if (curEvent.type == SDL_KEYDOWN) {
			switch (curEvent.key.keysym.sym)
			{
			case SDLK_UP:
			case SDLK_w:
				controller[MOVE_FORWARD] = true;
				break;

			case SDLK_DOWN:
			case SDLK_s:
				controller[MOVE_BACKWARD] = true;
				break;

			case SDLK_LEFT:
			case SDLK_a:
				controller[MOVE_LEFT] = true;
				break;

			case SDLK_RIGHT:
			case SDLK_d:
				controller[MOVE_RIGHT] = true;
				break;

			case SDLK_e:
				controller[MOVE_UP] = true;
				break;

			case SDLK_q:
				controller[MOVE_DOWN] = true;
				break;
			}
		}

		// handle key up events
		if (curEvent.type == SDL_KEYUP) {
			switch (curEvent.key.keysym.sym)
			{
			case SDLK_UP:
			case SDLK_w:
				controller[MOVE_FORWARD] = false;
				break;

			case SDLK_DOWN:
			case SDLK_s:
				controller[MOVE_BACKWARD] = false;
				break;

			case SDLK_LEFT:
			case SDLK_a:
				controller[MOVE_LEFT] = false;
				break;

			case SDLK_RIGHT:
			case SDLK_d:
				controller[MOVE_RIGHT] = false;
				break;

			case SDLK_e:
				controller[MOVE_UP] = false;
				break;

			case SDLK_q:
				controller[MOVE_DOWN] = false;
				break;
			}
		}

		if (curEvent.type == SDL_MOUSEMOTION)
		{
			motion += glm::vec2(curEvent.motion.xrel, curEvent.motion.yrel);
		} 
		else
		{
			motion = glm::vec2(0, 0);
		}
	}
}


void Player::render()
{

}

glm::vec3 Player::getPosition()
{
	return position;
}

glm::vec3 Player::getForward()
{
	return forward;
}

//Player::~Player()
//{
//}
