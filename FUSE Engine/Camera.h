#pragma once

#include <glm.hpp>
#include <gtx/transform.hpp>

class Camera
{
public:
	Camera(const glm::vec3 pos, float fov, float aspect, float znear, float zfar);
	~Camera();

	void setForwardRotation(glm::vec3 forwardRotation);
	glm::vec3 getPosition();
	void setPosition(glm::vec3 position);

	glm::mat4 getViewProjection() const;

private:
	glm::mat4 perspective;
	glm::vec3 position;
	glm::vec3 forwardRotation;
	glm::vec3 upRotation;
};

