/*
Author: Colton Ramos
Western Kentucky University

FuseEngine is the class that makes it all happen. Extend this class
to create a new game using the FUSE Engine. Once extended, the four
following abstract methods must be overridden:
- initialize
- update
- render
- deinitialize
*/

#pragma once

//Using SDL and standard IO
#include <SDL.h>
#include <glew.h>
#include "Timer.h"
#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>

class FuseEngine
{

public:
	FuseEngine();
	~FuseEngine();
	void run();
	SDL_GLContext glContext;
	bool quitApplication = false;

protected:
	char* WINDOW_TITLE = "Fuse Engine";
	
	std::ofstream debugFile;

	int SCREEN_HEIGHT = 768;
	int SCREEN_WIDTH = 1024;

	SDL_Window* window;				//The window we'll be rendering to

	SDL_Surface* screenSurface;		//The surface contained by the window

	int fps = 0;

	const int TARGET_FPS = 60;
	const int TARGET_TICKS_PER_FRAME = 1000 / TARGET_FPS;
	const int FPS_CHECK_FREQUENCY = 1000;

private:
	// FUSE Engine cycle management
	void initializeWindow();
	void fetchEvents();
	void drawWindow();
	void deinitializeWindow();

	// The four methods below are abstract methods for developer's implementation
	// See SampleGame.h and SampleGame.cpp for an example.
	
	// Used for initialization
	virtual void initialize() =0;

	// Called every update frame.
	virtual void update(Uint32 deltaTime, std::vector<SDL_Event> eventVector) = 0;

	// Called after every update frame.
	virtual void render() = 0;

	// Used to tear down 
	virtual void deinitialize() = 0;

	std::vector<SDL_Event> eventVector;

	Timer frameTimer;
	Timer fpsTimer;
	unsigned int numFrames = 0;

	void startTimers();
	void updateTimer();
	void wait();

	Uint32 deltaTime;
};

