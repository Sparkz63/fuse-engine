<?xml version="1.0"?><doc>
<members>
<!-- Discarding badly formed XML document comment for member 'M:_wassert(System.Char!System.Runtime.CompilerServices.IsConst*,System.Char!System.Runtime.CompilerServices.IsConst*,System.UInt32)'. -->
<member name="M:_wassert(System.Char!System.Runtime.CompilerServices.IsConst*,System.Char!System.Runtime.CompilerServices.IsConst*,System.UInt32)" decl="true" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2">
OpenGL Mathematics (glm.g-truc.net)

Copyright (c) 2005 - 2014 G-Truc Creation (www.g-truc.net)
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

@ref core
@file glm/fwd.hpp
@date 2013-03-30 / 2013-03-31
@author Christophe Riccio
OpenGL Mathematics (glm.g-truc.net)

Copyright (c) 2005 - 2014 G-Truc Creation (www.g-truc.net)
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

@ref core
@file glm/core/type_int.hpp
@date 2008-08-22 / 2013-03-30
@author Christophe Riccio
OpenGL Mathematics (glm.g-truc.net)

Copyright (c) 2005 - 2014 G-Truc Creation (www.g-truc.net)
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

@ref core
@file glm/core/setup.hpp
@date 2006-11-13 / 2013-03-30
@author Christophe Riccio
</member>
<member name="D:glm.lowp_int" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_int.hpp" line="99">
@addtogroup core_precision
@{
Low precision signed integer. 
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.3 Integers</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_int" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_int.hpp" line="109">
Medium precision signed integer. 
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.3 Integers</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_int" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_int.hpp" line="116">
High precision signed integer.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.3 Integers</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_uint" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_int.hpp" line="123">
Low precision unsigned integer. 
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.3 Integers</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_uint" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_int.hpp" line="130">
Medium precision unsigned integer. 
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.3 Integers</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_uint" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_int.hpp" line="137">
High precision unsigned integer. 
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.3 Integers</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.uint" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_int.hpp" line="168">
Unsigned integer type.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.3 Integers</a>
</member>
<member name="D:glm.lowp_float" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_float.hpp" line="45">
@addtogroup core_precision
@{
Low precision floating-point numbers. 
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.4 Floats</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_float" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_float.hpp" line="55">
Medium precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.4 Floats</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_float" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_float.hpp" line="62">
High precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.4 Floats</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="57">
@addtogroup core_precision
@{
2 components vector of high single-precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="67">
2 components vector of medium single-precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="74">
2 components vector of low single-precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_dvec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="81">
2 components vector of high double-precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_dvec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="88">
2 components vector of medium double-precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_dvec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="95">
2 components vector of low double-precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_ivec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="102">
2 components vector of high precision signed integer numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_ivec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="109">
2 components vector of medium precision signed integer numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_ivec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="116">
2 components vector of low precision signed integer numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_uvec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="123">
2 components vector of high precision unsigned integer numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_uvec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="130">
2 components vector of medium precision unsigned integer numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_uvec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="137">
2 components vector of low precision unsigned integer numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_bvec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="144">
2 components vector of high precision bool numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_bvec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="151">
2 components vector of medium precision bool numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_bvec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="158">
2 components vector of low precision bool numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="165">
@}
@addtogroup core_precision
@{
3 components vector of high single-precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="178">
3 components vector of medium single-precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="185">
3 components vector of low single-precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_dvec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="192">
3 components vector of high double-precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_dvec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="199">
3 components vector of medium double-precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_dvec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="206">
3 components vector of low double-precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_ivec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="213">
3 components vector of high precision signed integer numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_ivec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="220">
3 components vector of medium precision signed integer numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_ivec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="227">
3 components vector of low precision signed integer numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_uvec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="234">
3 components vector of high precision unsigned integer numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_uvec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="241">
3 components vector of medium precision unsigned integer numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_uvec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="248">
3 components vector of low precision unsigned integer numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_bvec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="255">
3 components vector of high precision bool numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_bvec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="261">
3 components vector of medium precision bool numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_bvec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="267">
3 components vector of low precision bool numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="273">
@}
@addtogroup core_precision
@{
4 components vector of high single-precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="284">
4 components vector of medium single-precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="290">
4 components vector of low single-precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_dvec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="296">
4 components vector of high double-precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_dvec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="302">
4 components vector of medium double-precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_dvec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="308">
4 components vector of low double-precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_ivec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="314">
4 components vector of high precision signed integer numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_ivec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="320">
4 components vector of medium precision signed integer numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_ivec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="326">
4 components vector of low precision signed integer numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_uvec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="332">
4 components vector of high precision unsigned integer numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_uvec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="338">
4 components vector of medium precision unsigned integer numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_uvec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="344">
4 components vector of low precision unsigned integer numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_bvec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="350">
4 components vector of high precision bool numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_bvec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="356">
4 components vector of medium precision bool numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_bvec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="362">
4 components vector of low precision bool numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="368">
@}
@addtogroup core_types
@{
2 components vector of floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
</member>
<member name="D:glm.vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="391">

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
</member>
<member name="D:glm.vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="396">

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
</member>
<member name="D:glm.dvec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="413">
2 components vector of double-precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
</member>
<member name="D:glm.dvec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="419">

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
</member>
<member name="D:glm.dvec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="424">

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
</member>
<member name="D:glm.ivec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="442">

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
</member>
<member name="D:glm.ivec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="447">

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
</member>
<member name="D:glm.ivec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="452">

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
</member>
<member name="D:glm.uvec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="469">
2 components vector of unsigned integer numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
</member>
<member name="D:glm.uvec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="474">
3 components vector of unsigned integer numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
</member>
<member name="D:glm.uvec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="479">
4 components vector of unsigned integer numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
</member>
<member name="D:glm.bvec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="498">

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
</member>
<member name="D:glm.bvec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="503">

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
</member>
<member name="D:glm.bvec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_vec.hpp" line="508">

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.5 Vectors</a>
</member>
<member name="D:glm.lowp_mat2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="57">
@addtogroup core_precision
@{
2 columns of 2 components matrix of low precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_mat2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="67">
2 columns of 2 components matrix of medium precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_mat2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="74">
2 columns of 2 components matrix of high precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_mat2x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="81">
2 columns of 2 components matrix of low precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_mat2x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="88">
2 columns of 2 components matrix of medium precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_mat2x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="95">
2 columns of 2 components matrix of high precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_mat2x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="102">
@}
@addtogroup core_precision
@{
2 columns of 3 components matrix of low precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_mat2x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="114">
2 columns of 3 components matrix of medium precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_mat2x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="121">
2 columns of 3 components matrix of high precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_mat2x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="128">
@}
@addtogroup core_precision
@{
2 columns of 4 components matrix of low precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_mat2x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="140">
2 columns of 4 components matrix of medium precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_mat2x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="147">
2 columns of 4 components matrix of high precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_mat3x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="154">
@}
@addtogroup core_precision
@{
3 columns of 2 components matrix of low precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_mat3x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="166">
3 columns of 2 components matrix of medium precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_mat3x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="173">
3 columns of 2 components matrix of high precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_mat3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="180">
@}
@addtogroup core_precision
@{
3 columns of 3 components matrix of low precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_mat3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="192">
3 columns of 3 components matrix of medium precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_mat3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="199">
3 columns of 3 components matrix of high precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_mat3x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="206">
3 columns of 3 components matrix of low precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_mat3x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="213">
3 columns of 3 components matrix of medium precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_mat3x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="220">
3 columns of 3 components matrix of high precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_mat3x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="227">
@}
@addtogroup core_precision
@{
3 columns of 4 components matrix of low precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_mat3x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="239">
3 columns of 4 components matrix of medium precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_mat3x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="246">
3 columns of 4 components matrix of high precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_mat4x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="253">
@}
@addtogroup core_precision
@{
4 columns of 2 components matrix of low precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_mat4x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="265">
4 columns of 2 components matrix of medium precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_mat4x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="272">
4 columns of 2 components matrix of high precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_mat4x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="279">
@}
@addtogroup core_precision
@{
4 columns of 3 components matrix of low precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_mat4x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="291">
4 columns of 3 components matrix of medium precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_mat4x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="298">
4 columns of 3 components matrix of high precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_mat4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="305">
@}
@addtogroup core_precision
@{
4 columns of 4 components matrix of low precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_mat4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="318">
4 columns of 4 components matrix of medium precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_mat4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="325">
4 columns of 4 components matrix of high precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_mat4x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="332">
4 columns of 4 components matrix of low precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_mat4x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="339">
4 columns of 4 components matrix of medium precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_mat4x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="346">
4 columns of 4 components matrix of high precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mat2x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="353">
@}
@addtogroup core_types
@{

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
</member>
<member name="D:glm.mat2x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="388">

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
</member>
<member name="D:glm.mat2x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="393">

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
</member>
<member name="D:glm.mat3x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="398">

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
</member>
<member name="D:glm.mat3x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="403">

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
</member>
<member name="D:glm.mat3x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="408">

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
</member>
<member name="D:glm.mat4x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="413">

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
</member>
<member name="D:glm.mat4x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="418">

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
</member>
<member name="D:glm.mat4x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="423">

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
</member>
<member name="D:glm.mat2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="430">

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
</member>
<member name="D:glm.mat3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="435">

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
</member>
<member name="D:glm.mat4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="440">

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
</member>
<member name="D:glm.lowp_dmat2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="447">
@addtogroup core_precision
@{
2 columns of 2 components matrix of low precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_dmat2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="456">
2 columns of 2 components matrix of medium precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_dmat2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="462">
2 columns of 2 components matrix of high precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_dmat2x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="468">
2 columns of 2 components matrix of low precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_dmat2x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="474">
2 columns of 2 components matrix of medium precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_dmat2x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="480">
2 columns of 2 components matrix of high precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_dmat2x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="486">
@}
@addtogroup core_precision
@{
2 columns of 3 components matrix of low precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_dmat2x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="497">
2 columns of 3 components matrix of medium precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_dmat2x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="503">
2 columns of 3 components matrix of high precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_dmat2x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="509">
@}
@addtogroup core_precision
@{
2 columns of 4 components matrix of low precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_dmat2x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="520">
2 columns of 4 components matrix of medium precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_dmat2x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="526">
2 columns of 4 components matrix of high precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_dmat3x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="532">
@}
@addtogroup core_precision
@{
3 columns of 2 components matrix of low precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_dmat3x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="543">
3 columns of 2 components matrix of medium precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_dmat3x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="549">
3 columns of 2 components matrix of high precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_dmat3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="555">
@}
@addtogroup core_precision
@{
3 columns of 3 components matrix of low precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_dmat3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="566">
3 columns of 3 components matrix of medium precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_dmat3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="572">
3 columns of 3 components matrix of high precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_dmat3x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="578">
3 columns of 3 components matrix of low precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_dmat3x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="584">
3 columns of 3 components matrix of medium precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_dmat3x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="590">
3 columns of 3 components matrix of high precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_dmat3x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="596">
@}
@addtogroup core_precision
@{
3 columns of 4 components matrix of low precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_dmat3x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="607">
3 columns of 4 components matrix of medium precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_dmat3x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="613">
3 columns of 4 components matrix of high precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_dmat4x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="619">
@}
@addtogroup core_precision
@{
4 columns of 2 components matrix of low precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_dmat4x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="630">
4 columns of 2 components matrix of medium precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_dmat4x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="636">
4 columns of 2 components matrix of high precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_dmat4x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="642">
@}
@addtogroup core_precision
@{
4 columns of 3 components matrix of low precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_dmat4x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="653">
4 columns of 3 components matrix of medium precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_dmat4x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="659">
4 columns of 3 components matrix of high precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_dmat4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="665">
@}
@addtogroup core_precision
@{
4 columns of 4 components matrix of low precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_dmat4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="676">
4 columns of 4 components matrix of medium precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_dmat4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="682">
4 columns of 4 components matrix of high precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_dmat4x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="688">
4 columns of 4 components matrix of low precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_dmat4x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="694">
4 columns of 4 components matrix of medium precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_dmat4x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="700">
4 columns of 4 components matrix of high precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.dmat2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="706">
@}

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
</member>
<member name="D:glm.dmat3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="736">

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
</member>
<member name="D:glm.dmat4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="741">

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
</member>
<member name="D:glm.dmat2x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="746">

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
</member>
<member name="D:glm.dmat2x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="751">

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
</member>
<member name="D:glm.dmat2x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="756">

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
</member>
<member name="D:glm.dmat3x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="761">

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
</member>
<member name="D:glm.dmat3x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="765">
3 * 3 matrix of double-precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
</member>
<member name="D:glm.dmat3x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="770">
3 * 4 matrix of double-precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
</member>
<member name="D:glm.dmat4x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="775">
4 * 2 matrix of double-precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
</member>
<member name="D:glm.dmat4x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="780">
4 * 3 matrix of double-precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
</member>
<member name="D:glm.dmat4x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\type_mat.hpp" line="785">
4 * 4 matrix of double-precision floating-point numbers.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
</member>
<member name="D:glm.lowp_quat" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="46">
Quaternion of low single-precision floating-point numbers.

@see gtc_quaternion
</member>
<member name="D:glm.mediump_quat" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="51">
Quaternion of medium single-precision floating-point numbers.

@see gtc_quaternion
</member>
<member name="D:glm.highp_quat" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="56">
Quaternion of high single-precision floating-point numbers.

@see gtc_quaternion
</member>
<member name="D:glm.quat" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="68">
Quaternion of default single-precision floating-point numbers.
</member>
<member name="D:glm.lowp_fquat" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="72">
Quaternion of low single-precision floating-point numbers.

@see gtc_quaternion
</member>
<member name="D:glm.mediump_fquat" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="77">
Quaternion of medium single-precision floating-point numbers.

@see gtc_quaternion
</member>
<member name="D:glm.highp_fquat" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="82">
Quaternion of high single-precision floating-point numbers.

@see gtc_quaternion
</member>
<member name="D:glm.fquat" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="87">
Quaternion of default single-precision floating-point numbers.

@see gtc_quaternion
</member>
<member name="D:glm.lowp_dquat" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="93">
Quaternion of low double-precision floating-point numbers.

@see gtc_quaternion
</member>
<member name="D:glm.mediump_dquat" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="98">
Quaternion of medium double-precision floating-point numbers.

@see gtc_quaternion
</member>
<member name="D:glm.highp_dquat" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="103">
Quaternion of high double-precision floating-point numbers.

@see gtc_quaternion
</member>
<member name="D:glm.dquat" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="115">
Quaternion of default double-precision floating-point numbers.

@see gtc_quaternion
</member>
<member name="D:glm.lowp_int8" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="127">
@addtogroup gtc_type_precision
@{
Low precision 8 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_int16" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="134">
Low precision 16 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_int32" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="138">
Low precision 32 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_int64" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="142">
Low precision 64 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_int8_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="146">
Low precision 8 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_int16_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="150">
Low precision 16 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_int32_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="154">
Low precision 32 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_int64_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="158">
Low precision 64 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_i8" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="162">
Low precision 8 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_i16" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="166">
Low precision 16 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_i32" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="170">
Low precision 32 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_i64" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="174">
Low precision 64 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_int8" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="178">
Medium precision 8 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_int16" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="182">
Medium precision 16 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_int32" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="186">
Medium precision 32 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_int64" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="190">
Medium precision 64 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_int8_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="194">
Medium precision 8 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_int16_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="198">
Medium precision 16 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_int32_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="202">
Medium precision 32 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_int64_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="206">
Medium precision 64 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_i8" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="210">
Medium precision 8 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_i16" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="214">
Medium precision 16 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_i32" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="218">
Medium precision 32 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_i64" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="222">
Medium precision 64 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_int8" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="226">
High precision 8 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_int16" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="230">
High precision 16 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_int32" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="234">
High precision 32 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_int64" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="238">
High precision 64 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_int8_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="242">
High precision 8 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_int16_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="246">
High precision 16 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_int32_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="250">
32 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_int64_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="254">
High precision 64 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_i8" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="258">
High precision 8 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_i16" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="262">
High precision 16 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_i32" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="266">
High precision 32 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_i64" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="270">
High precision 64 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.int8" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="275">
8 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.int16" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="279">
16 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.int32" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="283">
32 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.int64" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="287">
64 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.int8_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="291">
8 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.int16_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="295">
16 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.int32_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="299">
32 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.int64_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="303">
64 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.i8" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="307">
8 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.i16" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="311">
16 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.i32" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="315">
32 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.i64" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="319">
64 bit signed integer type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_i8vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="325">
Low precision 8 bit signed integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_i8vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="329">
Low precision 8 bit signed integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_i8vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="333">
Low precision 8 bit signed integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_i8vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="337">
Low precision 8 bit signed integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_i8vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="342">
Medium precision 8 bit signed integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_i8vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="346">
Medium precision 8 bit signed integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_i8vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="350">
Medium precision 8 bit signed integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_i8vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="354">
Medium precision 8 bit signed integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_i8vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="359">
High precision 8 bit signed integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_i8vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="363">
High precision 8 bit signed integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_i8vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="367">
High precision 8 bit signed integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_i8vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="371">
High precision 8 bit signed integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.i8vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="386">
Default precision 8 bit signed integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.i8vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="390">
Default precision 8 bit signed integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.i8vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="394">
Default precision 8 bit signed integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.i8vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="398">
Default precision 8 bit signed integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_i16vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="404">
Low precision 16 bit signed integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_i16vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="408">
Low precision 16 bit signed integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_i16vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="412">
Low precision 16 bit signed integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_i16vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="416">
Low precision 16 bit signed integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_i16vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="421">
Medium precision 16 bit signed integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_i16vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="425">
Medium precision 16 bit signed integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_i16vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="429">
Medium precision 16 bit signed integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_i16vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="433">
Medium precision 16 bit signed integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_i16vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="438">
High precision 16 bit signed integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_i16vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="442">
High precision 16 bit signed integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_i16vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="446">
High precision 16 bit signed integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_i16vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="450">
High precision 16 bit signed integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.i16vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="466">
Default precision 16 bit signed integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.i16vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="470">
Default precision 16 bit signed integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.i16vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="474">
Default precision 16 bit signed integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.i16vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="478">
Default precision 16 bit signed integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_i32vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="484">
Low precision 32 bit signed integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_i32vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="488">
Low precision 32 bit signed integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_i32vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="492">
Low precision 32 bit signed integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_i32vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="496">
Low precision 32 bit signed integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_i32vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="501">
Medium precision 32 bit signed integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_i32vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="505">
Medium precision 32 bit signed integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_i32vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="509">
Medium precision 32 bit signed integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_i32vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="513">
Medium precision 32 bit signed integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_i32vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="518">
High precision 32 bit signed integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_i32vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="522">
High precision 32 bit signed integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_i32vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="526">
High precision 32 bit signed integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_i32vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="530">
High precision 32 bit signed integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.i32vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="545">
Default precision 32 bit signed integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.i32vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="549">
Default precision 32 bit signed integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.i32vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="553">
Default precision 32 bit signed integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.i32vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="557">
Default precision 32 bit signed integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_i32vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="563">
Low precision 32 bit signed integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_i32vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="567">
Low precision 32 bit signed integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_i32vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="571">
Low precision 32 bit signed integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_i32vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="575">
Low precision 32 bit signed integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_i32vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="580">
Medium precision 32 bit signed integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_i32vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="584">
Medium precision 32 bit signed integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_i32vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="588">
Medium precision 32 bit signed integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_i32vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="592">
Medium precision 32 bit signed integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_i32vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="597">
High precision 32 bit signed integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_i32vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="601">
High precision 32 bit signed integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_i32vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="605">
High precision 32 bit signed integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_i32vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="609">
High precision 32 bit signed integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.i32vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="624">
Default precision 32 bit signed integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.i32vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="628">
Default precision 32 bit signed integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.i32vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="632">
Default precision 32 bit signed integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.i32vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="636">
Default precision 32 bit signed integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_i64vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="643">
Low precision 64 bit signed integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_i64vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="647">
Low precision 64 bit signed integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_i64vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="651">
Low precision 64 bit signed integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_i64vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="655">
Low precision 64 bit signed integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_i64vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="660">
Medium precision 64 bit signed integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_i64vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="664">
Medium precision 64 bit signed integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_i64vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="668">
Medium precision 64 bit signed integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_i64vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="672">
Medium precision 64 bit signed integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_i64vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="677">
High precision 64 bit signed integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_i64vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="681">
High precision 64 bit signed integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_i64vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="685">
High precision 64 bit signed integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_i64vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="689">
High precision 64 bit signed integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.i64vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="704">
Default precision 64 bit signed integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.i64vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="708">
Default precision 64 bit signed integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.i64vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="712">
Default precision 64 bit signed integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.i64vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="716">
Default precision 64 bit signed integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_uint8" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="725">
Low precision 8 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_uint16" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="729">
Low precision 16 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_uint32" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="733">
Low precision 32 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_uint64" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="737">
Low precision 64 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_uint8_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="742">
Low precision 8 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_uint16_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="746">
Low precision 16 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_uint32_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="750">
Low precision 32 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_uint64_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="754">
Low precision 64 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_u8" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="759">
Low precision 8 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_u16" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="763">
Low precision 16 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_u32" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="767">
Low precision 32 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_u64" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="771">
Low precision 64 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_uint8" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="777">
Medium precision 8 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_uint16" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="781">
Medium precision 16 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_uint32" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="785">
Medium precision 32 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_uint64" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="789">
Medium precision 64 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_uint8_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="793">
Medium precision 8 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_uint16_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="797">
Medium precision 16 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_uint32_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="801">
Medium precision 32 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_uint64_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="805">
Medium precision 64 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_u8" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="809">
Medium precision 8 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_u16" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="813">
Medium precision 16 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_u32" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="817">
Medium precision 32 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_u64" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="821">
Medium precision 64 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_uint8" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="827">
Medium precision 8 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_uint16" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="831">
Medium precision 16 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_uint32" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="835">
Medium precision 32 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_uint64" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="839">
Medium precision 64 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_uint8_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="843">
Medium precision 8 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_uint16_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="847">
Medium precision 16 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_uint32_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="851">
Medium precision 32 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_uint64_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="855">
Medium precision 64 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_u8" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="859">
Medium precision 8 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_u16" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="863">
Medium precision 16 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_u32" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="867">
Medium precision 32 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_u64" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="871">
Medium precision 64 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.uint8" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="877">
8 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.uint16" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="881">
16 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.uint32" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="885">
32 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.uint64" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="889">
64 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.uint8_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="893">
8 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.uint16_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="897">
16 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.uint32_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="901">
32 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.uint64_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="905">
64 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.u8" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="909">
8 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.u16" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="913">
16 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.u32" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="917">
32 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.u64" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="921">
64 bit unsigned integer type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_u8vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="928">
Low precision 8 bit unsigned integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_u8vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="932">
Low precision 8 bit unsigned integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_u8vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="936">
Low precision 8 bit unsigned integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_u8vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="940">
Low precision 8 bit unsigned integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_u8vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="945">
Medium precision 8 bit unsigned integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_u8vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="949">
Medium precision 8 bit unsigned integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_u8vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="953">
Medium precision 8 bit unsigned integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_u8vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="957">
Medium precision 8 bit unsigned integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_u8vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="962">
High precision 8 bit unsigned integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_u8vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="966">
High precision 8 bit unsigned integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_u8vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="970">
High precision 8 bit unsigned integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_u8vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="974">
High precision 8 bit unsigned integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.u8vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="989">
Default precision 8 bit unsigned integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.u8vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="993">
Default precision 8 bit unsigned integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.u8vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="997">
Default precision 8 bit unsigned integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.u8vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1001">
Default precision 8 bit unsigned integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_u16vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1007">
Low precision 16 bit unsigned integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_u16vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1011">
Low precision 16 bit unsigned integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_u16vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1015">
Low precision 16 bit unsigned integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_u16vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1019">
Low precision 16 bit unsigned integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_u16vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1024">
Medium precision 16 bit unsigned integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_u16vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1028">
Medium precision 16 bit unsigned integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_u16vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1032">
Medium precision 16 bit unsigned integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_u16vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1036">
Medium precision 16 bit unsigned integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_u16vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1041">
High precision 16 bit unsigned integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_u16vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1045">
High precision 16 bit unsigned integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_u16vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1049">
High precision 16 bit unsigned integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_u16vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1053">
High precision 16 bit unsigned integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.u16vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1069">
Default precision 16 bit unsigned integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.u16vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1073">
Default precision 16 bit unsigned integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.u16vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1077">
Default precision 16 bit unsigned integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.u16vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1081">
Default precision 16 bit unsigned integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_u32vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1087">
Low precision 32 bit unsigned integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_u32vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1091">
Low precision 32 bit unsigned integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_u32vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1095">
Low precision 32 bit unsigned integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_u32vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1099">
Low precision 32 bit unsigned integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_u32vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1104">
Medium precision 32 bit unsigned integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_u32vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1108">
Medium precision 32 bit unsigned integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_u32vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1112">
Medium precision 32 bit unsigned integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_u32vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1116">
Medium precision 32 bit unsigned integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_u32vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1121">
High precision 32 bit unsigned integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_u32vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1125">
High precision 32 bit unsigned integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_u32vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1129">
High precision 32 bit unsigned integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_u32vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1133">
High precision 32 bit unsigned integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.u32vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1148">
Default precision 32 bit unsigned integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.u32vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1152">
Default precision 32 bit unsigned integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.u32vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1156">
Default precision 32 bit unsigned integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.u32vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1160">
Default precision 32 bit unsigned integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_u32vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1166">
Low precision 32 bit unsigned integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_u32vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1170">
Low precision 32 bit unsigned integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_u32vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1174">
Low precision 32 bit unsigned integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_u32vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1178">
Low precision 32 bit unsigned integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_u32vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1183">
Medium precision 32 bit unsigned integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_u32vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1187">
Medium precision 32 bit unsigned integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_u32vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1191">
Medium precision 32 bit unsigned integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_u32vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1195">
Medium precision 32 bit unsigned integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_u32vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1200">
High precision 32 bit unsigned integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_u32vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1204">
High precision 32 bit unsigned integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_u32vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1208">
High precision 32 bit unsigned integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_u32vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1212">
High precision 32 bit unsigned integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.u32vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1227">
Default precision 32 bit unsigned integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.u32vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1231">
Default precision 32 bit unsigned integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.u32vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1235">
Default precision 32 bit unsigned integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.u32vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1239">
Default precision 32 bit unsigned integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_u64vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1246">
Low precision 64 bit unsigned integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_u64vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1250">
Low precision 64 bit unsigned integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_u64vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1254">
Low precision 64 bit unsigned integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_u64vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1258">
Low precision 64 bit unsigned integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_u64vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1263">
Medium precision 64 bit unsigned integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_u64vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1267">
Medium precision 64 bit unsigned integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_u64vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1271">
Medium precision 64 bit unsigned integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_u64vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1275">
Medium precision 64 bit unsigned integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_u64vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1280">
High precision 64 bit unsigned integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_u64vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1284">
High precision 64 bit unsigned integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_u64vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1288">
High precision 64 bit unsigned integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.highp_u64vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1292">
High precision 64 bit unsigned integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.u64vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1307">
Default precision 64 bit unsigned integer scalar type.
@see gtc_type_precision
</member>
<member name="D:glm.u64vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1311">
Default precision 64 bit unsigned integer vector of 2 components type.
@see gtc_type_precision
</member>
<member name="D:glm.u64vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1315">
Default precision 64 bit unsigned integer vector of 3 components type.
@see gtc_type_precision
</member>
<member name="D:glm.u64vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1319">
Default precision 64 bit unsigned integer vector of 4 components type.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_float32" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1328">
Low 32 bit single-precision floating-point scalar.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_float64" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1332">
Low 64 bit double-precision floating-point scalar.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_float32_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1336">
Low 32 bit single-precision floating-point scalar.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_float64_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1340">
Low 64 bit double-precision floating-point scalar.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f32" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1344">
Low 32 bit single-precision floating-point scalar.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f64" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1348">
Low 64 bit double-precision floating-point scalar.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_float32" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1352">
Low 32 bit single-precision floating-point scalar.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_float64" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1356">
Low 64 bit double-precision floating-point scalar.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_float32_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1360">
Low 32 bit single-precision floating-point scalar.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_float64_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1364">
Low 64 bit double-precision floating-point scalar.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f32" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1368">
Low 32 bit single-precision floating-point scalar.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f64" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1372">
Low 64 bit double-precision floating-point scalar.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_float32" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1377">
Low 32 bit single-precision floating-point scalar.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_float64" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1381">
Low 64 bit double-precision floating-point scalar.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_float32_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1385">
Low 32 bit single-precision floating-point scalar.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_float64_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1389">
Low 64 bit double-precision floating-point scalar.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f32" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1393">
Low 32 bit single-precision floating-point scalar.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f64" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1397">
Low 64 bit double-precision floating-point scalar.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_float32" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1402">
Medium 32 bit single-precision floating-point scalar.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_float64" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1406">
Medium 64 bit double-precision floating-point scalar.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_float32_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1410">
Medium 32 bit single-precision floating-point scalar.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_float64_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1414">
Medium 64 bit double-precision floating-point scalar.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_f32" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1418">
Medium 32 bit single-precision floating-point scalar.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_f64" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1422">
Medium 64 bit double-precision floating-point scalar.
@see gtc_type_precision
</member>
<member name="D:glm.highp_float32" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1427">
High 32 bit single-precision floating-point scalar.
@see gtc_type_precision
</member>
<member name="D:glm.highp_float64" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1431">
High 64 bit double-precision floating-point scalar.
@see gtc_type_precision
</member>
<member name="D:glm.highp_float32_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1435">
High 32 bit single-precision floating-point scalar.
@see gtc_type_precision
</member>
<member name="D:glm.highp_float64_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1439">
High 64 bit double-precision floating-point scalar.
@see gtc_type_precision
</member>
<member name="D:glm.highp_f32" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1443">
High 32 bit single-precision floating-point scalar.
@see gtc_type_precision
</member>
<member name="D:glm.highp_f64" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1447">
High 64 bit double-precision floating-point scalar.
@see gtc_type_precision
</member>
<member name="D:glm.float32" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1505">
Default 32 bit single-precision floating-point scalar.
@see gtc_type_precision
</member>
<member name="D:glm.float64" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1509">
Default 64 bit double-precision floating-point scalar.
@see gtc_type_precision
</member>
<member name="D:glm.float32_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1513">
Default 32 bit single-precision floating-point scalar.
@see gtc_type_precision
</member>
<member name="D:glm.float64_t" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1517">
Default 64 bit double-precision floating-point scalar.
@see gtc_type_precision
</member>
<member name="D:glm.f32" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1521">
Default 32 bit single-precision floating-point scalar.
@see gtc_type_precision
</member>
<member name="D:glm.f64" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1525">
Default 64 bit double-precision floating-point scalar.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1531">
Low single-precision floating-point vector of 1 component.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1535">
Low single-precision floating-point vector of 2 components.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1539">
Low single-precision floating-point vector of 3 components.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1543">
Low single-precision floating-point vector of 4 components.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_fvec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1547">
Low single-precision floating-point vector of 1 component.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_fvec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1551">
Low single-precision floating-point vector of 2 components.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_fvec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1555">
Low single-precision floating-point vector of 3 components.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_fvec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1559">
Low single-precision floating-point vector of 4 components.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1565">
Medium single-precision floating-point vector of 1 component.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1569">
Medium Single-precision floating-point vector of 2 components.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1573">
Medium Single-precision floating-point vector of 3 components.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1577">
Medium Single-precision floating-point vector of 4 components.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_fvec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1581">
Medium single-precision floating-point vector of 1 component.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_fvec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1585">
Medium Single-precision floating-point vector of 2 components.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_fvec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1589">
Medium Single-precision floating-point vector of 3 components.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_fvec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1593">
Medium Single-precision floating-point vector of 4 components.
@see gtc_type_precision
</member>
<member name="D:glm.highp_vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1599">
High single-precision floating-point vector of 1 component.
@see gtc_type_precision
</member>
<member name="D:glm.highp_vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1603">
High Single-precision floating-point vector of 2 components.
@see gtc_type_precision
</member>
<member name="D:glm.highp_vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1607">
High Single-precision floating-point vector of 3 components.
@see gtc_type_precision
</member>
<member name="D:glm.highp_vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1611">
High Single-precision floating-point vector of 4 components.
@see gtc_type_precision
</member>
<member name="D:glm.highp_fvec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1615">
High single-precision floating-point vector of 1 component.
@see gtc_type_precision
</member>
<member name="D:glm.highp_fvec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1619">
High Single-precision floating-point vector of 2 components.
@see gtc_type_precision
</member>
<member name="D:glm.highp_fvec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1623">
High Single-precision floating-point vector of 3 components.
@see gtc_type_precision
</member>
<member name="D:glm.highp_fvec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1627">
High Single-precision floating-point vector of 4 components.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f32vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1632">
Low single-precision floating-point vector of 1 component.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f32vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1636">
Low single-precision floating-point vector of 2 components.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f32vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1640">
Low single-precision floating-point vector of 3 components.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f32vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1644">
Low single-precision floating-point vector of 4 components.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_f32vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1648">
Medium single-precision floating-point vector of 1 component.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_f32vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1652">
Medium single-precision floating-point vector of 2 components.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_f32vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1656">
Medium single-precision floating-point vector of 3 components.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_f32vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1660">
Medium single-precision floating-point vector of 4 components.
@see gtc_type_precision
</member>
<member name="D:glm.highp_f32vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1664">
High single-precision floating-point vector of 1 component.
@see gtc_type_precision
</member>
<member name="D:glm.highp_f32vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1668">
High single-precision floating-point vector of 2 components.
@see gtc_type_precision
</member>
<member name="D:glm.highp_f32vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1672">
High single-precision floating-point vector of 3 components.
@see gtc_type_precision
</member>
<member name="D:glm.highp_f32vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1676">
High single-precision floating-point vector of 4 components.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f64vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1681">
Low double-precision floating-point vector of 1 component.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f64vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1685">
Low double-precision floating-point vector of 2 components.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f64vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1689">
Low double-precision floating-point vector of 3 components.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f64vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1693">
Low double-precision floating-point vector of 4 components.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_f64vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1697">
Medium double-precision floating-point vector of 1 component.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_f64vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1701">
Medium double-precision floating-point vector of 2 components.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_f64vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1705">
Medium double-precision floating-point vector of 3 components.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_f64vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1709">
Medium double-precision floating-point vector of 4 components.
@see gtc_type_precision
</member>
<member name="D:glm.highp_f64vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1713">
High double-precision floating-point vector of 1 component.
@see gtc_type_precision
</member>
<member name="D:glm.highp_f64vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1717">
High double-precision floating-point vector of 2 components.
@see gtc_type_precision
</member>
<member name="D:glm.highp_f64vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1721">
High double-precision floating-point vector of 3 components.
@see gtc_type_precision
</member>
<member name="D:glm.highp_f64vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1725">
High double-precision floating-point vector of 4 components.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_fmat2x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1733">
Low single-precision floating-point 1x1 matrix.
@see gtc_type_precision
Low single-precision floating-point 2x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_fmat2x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1741">
Low single-precision floating-point 2x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_fmat2x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1745">
Low single-precision floating-point 2x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_fmat3x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1749">
Low single-precision floating-point 3x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_fmat3x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1753">
Low single-precision floating-point 3x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_fmat3x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1757">
Low single-precision floating-point 3x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_fmat4x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1761">
Low single-precision floating-point 4x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_fmat4x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1765">
Low single-precision floating-point 4x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_fmat4x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1769">
Low single-precision floating-point 4x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_fmat2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1773">
Low single-precision floating-point 1x1 matrix.
@see gtc_type_precision
Low single-precision floating-point 2x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_fmat3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1781">
Low single-precision floating-point 3x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_fmat4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1785">
Low single-precision floating-point 4x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_fmat2x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1790">
Medium single-precision floating-point 1x1 matrix.
@see gtc_type_precision
Medium single-precision floating-point 2x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_fmat2x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1798">
Medium single-precision floating-point 2x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_fmat2x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1802">
Medium single-precision floating-point 2x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_fmat3x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1806">
Medium single-precision floating-point 3x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_fmat3x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1810">
Medium single-precision floating-point 3x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_fmat3x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1814">
Medium single-precision floating-point 3x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_fmat4x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1818">
Medium single-precision floating-point 4x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_fmat4x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1822">
Medium single-precision floating-point 4x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_fmat4x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1826">
Medium single-precision floating-point 4x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_fmat2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1830">
Medium single-precision floating-point 1x1 matrix.
@see gtc_type_precision
Medium single-precision floating-point 2x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_fmat3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1838">
Medium single-precision floating-point 3x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_fmat4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1842">
Medium single-precision floating-point 4x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.highp_fmat2x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1848">
High single-precision floating-point 1x1 matrix.
@see gtc_type_precision
High single-precision floating-point 2x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.highp_fmat2x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1856">
High single-precision floating-point 2x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.highp_fmat2x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1860">
High single-precision floating-point 2x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.highp_fmat3x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1864">
High single-precision floating-point 3x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.highp_fmat3x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1868">
High single-precision floating-point 3x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.highp_fmat3x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1872">
High single-precision floating-point 3x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.highp_fmat4x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1876">
High single-precision floating-point 4x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.highp_fmat4x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1880">
High single-precision floating-point 4x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.highp_fmat4x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1884">
High single-precision floating-point 4x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.highp_fmat2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1888">
High single-precision floating-point 1x1 matrix.
@see gtc_type_precision
High single-precision floating-point 2x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.highp_fmat3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1896">
High single-precision floating-point 3x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.highp_fmat4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1900">
High single-precision floating-point 4x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f32mat2x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1905">
Low single-precision floating-point 1x1 matrix.
@see gtc_type_precision
Low single-precision floating-point 2x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f32mat2x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1913">
Low single-precision floating-point 2x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f32mat2x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1917">
Low single-precision floating-point 2x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f32mat3x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1921">
Low single-precision floating-point 3x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f32mat3x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1925">
Low single-precision floating-point 3x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f32mat3x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1929">
Low single-precision floating-point 3x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f32mat4x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1933">
Low single-precision floating-point 4x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f32mat4x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1937">
Low single-precision floating-point 4x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f32mat4x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1941">
Low single-precision floating-point 4x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f32mat2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1945">
Low single-precision floating-point 1x1 matrix.
@see gtc_type_precision
Low single-precision floating-point 2x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f32mat3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1953">
Low single-precision floating-point 3x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f32mat4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1957">
Low single-precision floating-point 4x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_f32mat2x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1963">
High single-precision floating-point 1x1 matrix.
@see gtc_type_precision
Low single-precision floating-point 2x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_f32mat2x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1971">
Medium single-precision floating-point 2x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_f32mat2x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1975">
Medium single-precision floating-point 2x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_f32mat3x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1979">
Medium single-precision floating-point 3x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_f32mat3x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1983">
Medium single-precision floating-point 3x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_f32mat3x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1987">
Medium single-precision floating-point 3x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_f32mat4x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1991">
Medium single-precision floating-point 4x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_f32mat4x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1995">
Medium single-precision floating-point 4x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_f32mat4x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="1999">
Medium single-precision floating-point 4x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_f32mat2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2003">
Medium single-precision floating-point 1x1 matrix.
@see gtc_type_precision
Medium single-precision floating-point 2x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_f32mat3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2011">
Medium single-precision floating-point 3x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_f32mat4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2015">
Medium single-precision floating-point 4x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.highp_f32mat2x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2022">
High single-precision floating-point 1x1 matrix.
@see gtc_type_precision
High single-precision floating-point 2x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.highp_f32mat2x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2030">
High single-precision floating-point 2x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.highp_f32mat2x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2034">
High single-precision floating-point 2x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.highp_f32mat3x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2038">
High single-precision floating-point 3x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.highp_f32mat3x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2042">
High single-precision floating-point 3x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.highp_f32mat3x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2046">
High single-precision floating-point 3x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.highp_f32mat4x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2050">
High single-precision floating-point 4x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.highp_f32mat4x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2054">
High single-precision floating-point 4x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.highp_f32mat4x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2058">
High single-precision floating-point 4x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.highp_f32mat2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2062">
High single-precision floating-point 1x1 matrix.
@see gtc_type_precision
High single-precision floating-point 2x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.highp_f32mat3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2070">
High single-precision floating-point 3x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.highp_f32mat4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2074">
High single-precision floating-point 4x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f64mat2x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2080">
Low double-precision floating-point 1x1 matrix.
@see gtc_type_precision
Low double-precision floating-point 2x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f64mat2x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2088">
Low double-precision floating-point 2x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f64mat2x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2092">
Low double-precision floating-point 2x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f64mat3x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2096">
Low double-precision floating-point 3x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f64mat3x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2100">
Low double-precision floating-point 3x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f64mat3x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2104">
Low double-precision floating-point 3x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f64mat4x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2108">
Low double-precision floating-point 4x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f64mat4x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2112">
Low double-precision floating-point 4x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f64mat4x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2116">
Low double-precision floating-point 4x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f64mat2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2120">
Low double-precision floating-point 1x1 matrix.
@see gtc_type_precision
Low double-precision floating-point 2x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f64mat3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2128">
Low double-precision floating-point 3x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f64mat4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2132">
Low double-precision floating-point 4x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_f64mat2x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2138">
Medium double-precision floating-point 1x1 matrix.
@see gtc_type_precision
Medium double-precision floating-point 2x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_f64mat2x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2146">
Medium double-precision floating-point 2x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_f64mat2x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2150">
Medium double-precision floating-point 2x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_f64mat3x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2154">
Medium double-precision floating-point 3x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_f64mat3x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2158">
Medium double-precision floating-point 3x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_f64mat3x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2162">
Medium double-precision floating-point 3x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_f64mat4x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2166">
Medium double-precision floating-point 4x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_f64mat4x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2170">
Medium double-precision floating-point 4x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_f64mat4x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2174">
Medium double-precision floating-point 4x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_f64mat2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2178">
Medium double-precision floating-point 1x1 matrix.
@see gtc_type_precision
Medium double-precision floating-point 2x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_f64mat3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2186">
Medium double-precision floating-point 3x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_f64mat4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2190">
Medium double-precision floating-point 4x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.highp_f64mat2x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2194">
High double-precision floating-point 1x1 matrix.
@see gtc_type_precision
High double-precision floating-point 2x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.highp_f64mat2x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2202">
High double-precision floating-point 2x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.highp_f64mat2x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2206">
High double-precision floating-point 2x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.highp_f64mat3x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2210">
High double-precision floating-point 3x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.highp_f64mat3x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2214">
High double-precision floating-point 3x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.highp_f64mat3x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2218">
High double-precision floating-point 3x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.highp_f64mat4x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2222">
High double-precision floating-point 4x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.highp_f64mat4x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2226">
High double-precision floating-point 4x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.highp_f64mat4x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2230">
High double-precision floating-point 4x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.highp_f64mat2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2234">
High double-precision floating-point 1x1 matrix.
@see gtc_type_precision
High double-precision floating-point 2x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.highp_f64mat3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2242">
High double-precision floating-point 3x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.highp_f64mat4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2246">
High double-precision floating-point 4x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f32quat" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2253">
Low single-precision floating-point quaternion.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_f64quat" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2257">
Low double-precision floating-point quaternion.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_f32quat" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2261">
Medium single-precision floating-point quaternion.
@see gtc_type_precision
</member>
<member name="D:glm.mediump_f64quat" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2265">
Medium double-precision floating-point quaternion.
@see gtc_type_precision
</member>
<member name="D:glm.highp_f32quat" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2269">
High single-precision floating-point quaternion.
@see gtc_type_precision
</member>
<member name="D:glm.highp_f64quat" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2273">
High double-precision floating-point quaternion.
@see gtc_type_precision
</member>
<member name="D:glm.fvec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2351">
Default single-precision floating-point vector of 1 components.
@see gtc_type_precision
</member>
<member name="D:glm.fvec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2355">
Default single-precision floating-point vector of 2 components.
@see gtc_type_precision
</member>
<member name="D:glm.fvec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2359">
Default single-precision floating-point vector of 3 components.
@see gtc_type_precision
</member>
<member name="D:glm.fvec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2363">
Default single-precision floating-point vector of 4 components.
@see gtc_type_precision
</member>
<member name="D:glm.fmat2x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2367">
Default single-precision floating-point 2x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.fmat2x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2371">
Default single-precision floating-point 2x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.fmat2x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2375">
Default single-precision floating-point 2x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.fmat3x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2379">
Default single-precision floating-point 3x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.fmat3x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2383">
Default single-precision floating-point 3x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.fmat3x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2387">
Default single-precision floating-point 3x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.fmat4x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2391">
Default single-precision floating-point 4x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.fmat4x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2395">
Default single-precision floating-point 4x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.fmat4x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2399">
Default single-precision floating-point 4x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.fmat2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2403">
Default single-precision floating-point 2x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.fmat3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2407">
Default single-precision floating-point 3x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.fmat4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2411">
Default single-precision floating-point 4x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.fquat" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2415">
Default single-precision floating-point quaternion.
@see gtc_type_precision
</member>
<member name="D:glm.f32vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2421">
Default single-precision floating-point vector of 1 components.
@see gtc_type_precision
</member>
<member name="D:glm.f32vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2425">
Default single-precision floating-point vector of 2 components.
@see gtc_type_precision
</member>
<member name="D:glm.f32vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2429">
Default single-precision floating-point vector of 3 components.
@see gtc_type_precision
</member>
<member name="D:glm.f32vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2433">
Default single-precision floating-point vector of 4 components.
@see gtc_type_precision
</member>
<member name="D:glm.f32mat2x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2437">
Default single-precision floating-point 2x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.f32mat2x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2441">
Default single-precision floating-point 2x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.f32mat2x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2445">
Default single-precision floating-point 2x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.f32mat3x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2449">
Default single-precision floating-point 3x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.f32mat3x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2453">
Default single-precision floating-point 3x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.f32mat3x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2457">
Default single-precision floating-point 3x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.f32mat4x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2461">
Default single-precision floating-point 4x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.f32mat4x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2465">
Default single-precision floating-point 4x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.f32mat4x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2469">
Default single-precision floating-point 4x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.f32mat2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2473">
Default single-precision floating-point 2x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.f32mat3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2477">
Default single-precision floating-point 3x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.f32mat4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2481">
Default single-precision floating-point 4x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.f32quat" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2485">
Default single-precision floating-point quaternion.
@see gtc_type_precision
</member>
<member name="D:glm.f64vec1" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2528">
Default double-precision floating-point vector of 1 components.
@see gtc_type_precision
</member>
<member name="D:glm.f64vec2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2532">
Default double-precision floating-point vector of 2 components.
@see gtc_type_precision
</member>
<member name="D:glm.f64vec3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2536">
Default double-precision floating-point vector of 3 components.
@see gtc_type_precision
</member>
<member name="D:glm.f64vec4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2540">
Default double-precision floating-point vector of 4 components.
@see gtc_type_precision
</member>
<member name="D:glm.f64mat2x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2544">
Default double-precision floating-point 2x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.f64mat2x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2548">
Default double-precision floating-point 2x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.f64mat2x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2552">
Default double-precision floating-point 2x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.f64mat3x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2556">
Default double-precision floating-point 3x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.f64mat3x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2560">
Default double-precision floating-point 3x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.f64mat3x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2564">
Default double-precision floating-point 3x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.f64mat4x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2568">
Default double-precision floating-point 4x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.f64mat4x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2572">
Default double-precision floating-point 4x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.f64mat4x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2576">
Default double-precision floating-point 4x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.f64mat2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2580">
Default double-precision floating-point 2x2 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.f64mat3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2584">
Default double-precision floating-point 3x3 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.f64mat4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2588">
Default double-precision floating-point 4x4 matrix.
@see gtc_type_precision
</member>
<member name="D:glm.f64quat" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\fwd.hpp" line="2592">
Default double-precision floating-point quaternion.
@see gtc_type_precision
</member>
<member name="D:glm.lowp_mat2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\mat2x2.hpp" line="36">
2 columns of 2 components matrix of low precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_mat2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\mat2x2.hpp" line="43">
2 columns of 2 components matrix of medium precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_mat2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\mat2x2.hpp" line="50">
2 columns of 2 components matrix of high precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_mat2x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\mat2x2.hpp" line="57">
2 columns of 2 components matrix of low precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_mat2x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\mat2x2.hpp" line="64">
2 columns of 2 components matrix of medium precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_mat2x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\mat2x2.hpp" line="71">
2 columns of 2 components matrix of high precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_mat2x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\mat2x3.hpp" line="36">
2 columns of 3 components matrix of low precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_mat2x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\mat2x3.hpp" line="43">
2 columns of 3 components matrix of medium precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_mat2x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\mat2x3.hpp" line="50">
2 columns of 3 components matrix of high precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_mat2x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\mat2x4.hpp" line="36">
2 columns of 4 components matrix of low precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_mat2x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\mat2x4.hpp" line="43">
2 columns of 4 components matrix of medium precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_mat2x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\mat2x4.hpp" line="50">
2 columns of 4 components matrix of high precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_mat3x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\mat3x2.hpp" line="36">
3 columns of 2 components matrix of low precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_mat3x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\mat3x2.hpp" line="43">
3 columns of 2 components matrix of medium precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_mat3x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\mat3x2.hpp" line="50">
3 columns of 2 components matrix of high precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_mat3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\mat3x3.hpp" line="36">
3 columns of 3 components matrix of low precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_mat3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\mat3x3.hpp" line="43">
3 columns of 3 components matrix of medium precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_mat3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\mat3x3.hpp" line="50">
3 columns of 3 components matrix of high precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_mat3x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\mat3x3.hpp" line="57">
3 columns of 3 components matrix of low precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_mat3x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\mat3x3.hpp" line="64">
3 columns of 3 components matrix of medium precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_mat3x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\mat3x3.hpp" line="71">
3 columns of 3 components matrix of high precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_mat3x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\mat3x4.hpp" line="36">
3 columns of 4 components matrix of low precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_mat3x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\mat3x4.hpp" line="43">
3 columns of 4 components matrix of medium precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_mat3x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\mat3x4.hpp" line="50">
3 columns of 4 components matrix of high precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_mat4x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\mat4x2.hpp" line="36">
4 columns of 2 components matrix of low precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_mat4x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\mat4x2.hpp" line="43">
4 columns of 2 components matrix of medium precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_mat4x2" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\mat4x2.hpp" line="50">
4 columns of 2 components matrix of high precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_mat4x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\mat4x3.hpp" line="36">
4 columns of 3 components matrix of low precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_mat4x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\mat4x3.hpp" line="43">
4 columns of 3 components matrix of medium precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_mat4x3" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\mat4x3.hpp" line="50">
4 columns of 3 components matrix of high precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_mat4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\mat4x4.hpp" line="36">
4 columns of 4 components matrix of low precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_mat4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\mat4x4.hpp" line="43">
4 columns of 4 components matrix of medium precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_mat4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\mat4x4.hpp" line="50">
4 columns of 4 components matrix of high precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.lowp_mat4x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\mat4x4.hpp" line="57">
4 columns of 4 components matrix of low precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.mediump_mat4x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\mat4x4.hpp" line="64">
4 columns of 4 components matrix of medium precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="D:glm.highp_mat4x4" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\mat4x4.hpp" line="71">
4 columns of 4 components matrix of high precision floating-point numbers.
There is no guarantee on the actual precision.

@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.1.6 Matrices</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 4.7.2 Precision Qualifier</a>
</member>
<member name="M:glm.floatBitsToInt(System.Single!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="true" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\func_common.hpp" line="351">
Returns a signed integer value representing
the encoding of a floating-point value. The floating-point
value's bit-level representation is preserved.

@see <a href="http://www.opengl.org/sdk/docs/manglsl/xhtml/floatBitsToInt.xml">GLSL floatBitsToInt man page</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 8.3 Common Functions</a>
</member>
<member name="M:glm.floatBitsToUint(System.Single!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="true" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\func_common.hpp" line="368">
Returns a unsigned integer value representing
the encoding of a floating-point value. The floatingpoint
value's bit-level representation is preserved.

@see <a href="http://www.opengl.org/sdk/docs/manglsl/xhtml/floatBitsToUint.xml">GLSL floatBitsToUint man page</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 8.3 Common Functions</a>
</member>
<member name="M:glm.intBitsToFloat(System.Int32!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="true" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\func_common.hpp" line="385">
Returns a floating-point value corresponding to a signed
integer encoding of a floating-point value.
If an inf or NaN is passed in, it will not signal, and the
resulting floating point value is unspecified. Otherwise,
the bit-level representation is preserved.

@see <a href="http://www.opengl.org/sdk/docs/manglsl/xhtml/intBitsToFloat.xml">GLSL intBitsToFloat man page</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 8.3 Common Functions</a>
</member>
<member name="M:glm.uintBitsToFloat(System.UInt32!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="true" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\func_common.hpp" line="406">
Returns a floating-point value corresponding to a
unsigned integer encoding of a floating-point value.
If an inf or NaN is passed in, it will not signal, and the
resulting floating point value is unspecified. Otherwise,
the bit-level representation is preserved.

@see <a href="http://www.opengl.org/sdk/docs/manglsl/xhtml/uintBitsToFloat.xml">GLSL uintBitsToFloat man page</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 8.3 Common Functions</a>
</member>
<member name="M:glm.packUnorm2x16(glm.detail.tvec2&lt;System.Single,0x0&gt;!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="true" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\func_packing.hpp" line="44">
@addtogroup core_func_packing
@{
First, converts each component of the normalized floating-point value v into 8- or 16-bit integer values. 
Then, the results are packed into the returned 32-bit unsigned integer.

The conversion for component c of v to fixed point is done as follows:
packUnorm2x16: round(clamp(c, 0, +1) * 65535.0) 

The first component of the vector will be written to the least significant bits of the output; 
the last component will be written to the most significant bits.

@see <a href="http://www.opengl.org/sdk/docs/manglsl/xhtml/packUnorm2x16.xml">GLSL packUnorm2x16 man page</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 8.4 Floating-Point Pack and Unpack Functions</a>
</member>
<member name="M:glm.packSnorm2x16(glm.detail.tvec2&lt;System.Single,0x0&gt;!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="true" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\func_packing.hpp" line="60">
First, converts each component of the normalized floating-point value v into 8- or 16-bit integer values. 
Then, the results are packed into the returned 32-bit unsigned integer.

The conversion for component c of v to fixed point is done as follows:
packSnorm2x16: round(clamp(v, -1, +1) * 32767.0)

The first component of the vector will be written to the least significant bits of the output; 
the last component will be written to the most significant bits.

@see <a href="http://www.opengl.org/sdk/docs/manglsl/xhtml/packSnorm2x16.xml">GLSL packSnorm2x16 man page</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 8.4 Floating-Point Pack and Unpack Functions</a>
</member>
<member name="M:glm.packUnorm4x8(glm.detail.tvec4&lt;System.Single,0x0&gt;!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="true" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\func_packing.hpp" line="73">
First, converts each component of the normalized floating-point value v into 8- or 16-bit integer values. 
Then, the results are packed into the returned 32-bit unsigned integer.

The conversion for component c of v to fixed point is done as follows:
packUnorm4x8:	round(clamp(c, 0, +1) * 255.0)

The first component of the vector will be written to the least significant bits of the output; 
the last component will be written to the most significant bits.

@see <a href="http://www.opengl.org/sdk/docs/manglsl/xhtml/packUnorm4x8.xml">GLSL packUnorm4x8 man page</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 8.4 Floating-Point Pack and Unpack Functions</a>
</member>
<member name="M:glm.packSnorm4x8(glm.detail.tvec4&lt;System.Single,0x0&gt;!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="true" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\func_packing.hpp" line="86">
First, converts each component of the normalized floating-point value v into 8- or 16-bit integer values. 
Then, the results are packed into the returned 32-bit unsigned integer.

The conversion for component c of v to fixed point is done as follows:
packSnorm4x8:	round(clamp(c, -1, +1) * 127.0) 

The first component of the vector will be written to the least significant bits of the output; 
the last component will be written to the most significant bits.

@see <a href="http://www.opengl.org/sdk/docs/manglsl/xhtml/packSnorm4x8.xml">GLSL packSnorm4x8 man page</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 8.4 Floating-Point Pack and Unpack Functions</a>
</member>
<member name="M:glm.unpackUnorm2x16(System.UInt32!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="true" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\func_packing.hpp" line="99">
First, unpacks a single 32-bit unsigned integer p into a pair of 16-bit unsigned integers, four 8-bit unsigned integers, or four 8-bit signed integers. 
Then, each component is converted to a normalized floating-point value to generate the returned two- or four-component vector.

The conversion for unpacked fixed-point value f to floating point is done as follows:
unpackUnorm2x16: f / 65535.0 

The first component of the returned vector will be extracted from the least significant bits of the input; 
the last component will be extracted from the most significant bits.

@see <a href="http://www.opengl.org/sdk/docs/manglsl/xhtml/unpackUnorm2x16.xml">GLSL unpackUnorm2x16 man page</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 8.4 Floating-Point Pack and Unpack Functions</a>
</member>
<member name="M:glm.unpackSnorm2x16(System.UInt32!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="true" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\func_packing.hpp" line="112">
First, unpacks a single 32-bit unsigned integer p into a pair of 16-bit unsigned integers, four 8-bit unsigned integers, or four 8-bit signed integers. 
Then, each component is converted to a normalized floating-point value to generate the returned two- or four-component vector.

The conversion for unpacked fixed-point value f to floating point is done as follows:
unpackSnorm2x16: clamp(f / 32767.0, -1, +1)

The first component of the returned vector will be extracted from the least significant bits of the input; 
the last component will be extracted from the most significant bits.

@see <a href="http://www.opengl.org/sdk/docs/manglsl/xhtml/unpackSnorm2x16.xml">GLSL unpackSnorm2x16 man page</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 8.4 Floating-Point Pack and Unpack Functions</a>
</member>
<member name="M:glm.unpackUnorm4x8(System.UInt32!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="true" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\func_packing.hpp" line="125">
First, unpacks a single 32-bit unsigned integer p into a pair of 16-bit unsigned integers, four 8-bit unsigned integers, or four 8-bit signed integers. 
Then, each component is converted to a normalized floating-point value to generate the returned two- or four-component vector.

The conversion for unpacked fixed-point value f to floating point is done as follows:
unpackUnorm4x8: f / 255.0

The first component of the returned vector will be extracted from the least significant bits of the input; 
the last component will be extracted from the most significant bits.

@see <a href="http://www.opengl.org/sdk/docs/manglsl/xhtml/unpackUnorm4x8.xml">GLSL unpackUnorm4x8 man page</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 8.4 Floating-Point Pack and Unpack Functions</a>
</member>
<member name="M:glm.unpackSnorm4x8(System.UInt32!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="true" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\func_packing.hpp" line="138">
First, unpacks a single 32-bit unsigned integer p into a pair of 16-bit unsigned integers, four 8-bit unsigned integers, or four 8-bit signed integers. 
Then, each component is converted to a normalized floating-point value to generate the returned two- or four-component vector.

The conversion for unpacked fixed-point value f to floating point is done as follows:
unpackSnorm4x8: clamp(f / 127.0, -1, +1)

The first component of the returned vector will be extracted from the least significant bits of the input; 
the last component will be extracted from the most significant bits.

@see <a href="http://www.opengl.org/sdk/docs/manglsl/xhtml/unpackSnorm4x8.xml">GLSL unpackSnorm4x8 man page</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 8.4 Floating-Point Pack and Unpack Functions</a>
</member>
<member name="M:glm.packDouble2x32(glm.detail.tvec2&lt;System.UInt32,0x0&gt;!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="true" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\func_packing.hpp" line="151">
Returns a double-precision value obtained by packing the components of v into a 64-bit value. 
If an IEEE 754 Inf or NaN is created, it will not signal, and the resulting floating point value is unspecified. 
Otherwise, the bit- level representation of v is preserved. 
The first vector component specifies the 32 least significant bits; 
the second component specifies the 32 most significant bits.

@see <a href="http://www.opengl.org/sdk/docs/manglsl/xhtml/packDouble2x32.xml">GLSL packDouble2x32 man page</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 8.4 Floating-Point Pack and Unpack Functions</a>
</member>
<member name="M:glm.unpackDouble2x32(System.Double!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="true" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\func_packing.hpp" line="161">
Returns a two-component unsigned integer vector representation of v. 
The bit-level representation of v is preserved. 
The first component of the vector contains the 32 least significant bits of the double; 
the second component consists the 32 most significant bits.

@see <a href="http://www.opengl.org/sdk/docs/manglsl/xhtml/unpackDouble2x32.xml">GLSL unpackDouble2x32 man page</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 8.4 Floating-Point Pack and Unpack Functions</a>
</member>
<member name="M:glm.packHalf2x16(glm.detail.tvec2&lt;System.Single,0x0&gt;!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="true" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\func_packing.hpp" line="170">
Returns an unsigned integer obtained by converting the components of a two-component floating-point vector 
to the 16-bit floating-point representation found in the OpenGL Specification, 
and then packing these two 16- bit integers into a 32-bit unsigned integer.
The first vector component specifies the 16 least-significant bits of the result; 
the second component specifies the 16 most-significant bits.

@see <a href="http://www.opengl.org/sdk/docs/manglsl/xhtml/packHalf2x16.xml">GLSL packHalf2x16 man page</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 8.4 Floating-Point Pack and Unpack Functions</a>
</member>
<member name="M:glm.unpackHalf2x16(System.UInt32!System.Runtime.CompilerServices.IsConst*!System.Runtime.CompilerServices.IsImplicitlyDereferenced)" decl="true" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\func_packing.hpp" line="180">
Returns a two-component floating-point vector with components obtained by unpacking a 32-bit unsigned integer into a pair of 16-bit values, 
interpreting those values as 16-bit floating-point numbers according to the OpenGL Specification, 
and converting them to 32-bit floating-point values.
The first component of the vector is obtained from the 16 least-significant bits of v; 
the second component is obtained from the 16 most-significant bits of v.

@see <a href="http://www.opengl.org/sdk/docs/manglsl/xhtml/unpackHalf2x16.xml">GLSL unpackHalf2x16 man page</a>
@see <a href="http://www.opengl.org/registry/doc/GLSLangSpec.4.20.8.pdf">GLSL 4.20.8 specification, section 8.4 Floating-Point Pack and Unpack Functions</a>
</member>
<member name="T:Camera" decl="false" source="c:\users\colton\dropbox\researchcramos\fuse-engine\fuse engine\include\glm\detail\_fixes.hpp" line="2">
OpenGL Mathematics (glm.g-truc.net)

Copyright (c) 2005 - 2014 G-Truc Creation (www.g-truc.net)
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

@ref core
@file glm/core/_fixes.hpp
@date 2011-02-21 / 2011-11-22
@author Christophe Riccio
</member>
</members>
</doc>