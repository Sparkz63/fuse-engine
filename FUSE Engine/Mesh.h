#pragma once

#include <glm.hpp>
#include <glew.h>
#include <vector>
#include "tiny_obj_loader.h"
#include "Texture.h"
#include <iostream>

/*
Represents a single vertex of our mesh, including a position,
texture coordinate, and normal.*/
class Vertex 
{
public:
	Vertex(const glm::vec3 &pos, const glm::vec2 &texCoord, const glm::vec3 &normal = glm::vec3(0, 0, 0))
	{
		this->pos = pos;
		this->texCoord = texCoord;
		this->normal = normal;
	}

	inline glm::vec3 *getPos() { return &pos; }
	inline glm::vec2 *getTexCoord() { return &texCoord; }
	inline glm::vec3 *getNormal() { return &normal; }

protected:
private:
	glm::vec3 pos;
	glm::vec2 texCoord;
	glm::vec3 normal;
};

/*
A 3D mesh represented by a collection of Vertices and texture datas.
*/
class Mesh
{
	enum
	{
		POSITION_VB,
		TEXCOORD_VB,
		NORMAL_VB,

		INDEX_VB,

		NUM_BUFFERS
	};

	struct  VertexArray {
		unsigned int drawCount;
		GLuint vertexArrayObject;
		GLuint vertexArrayBuffers[Mesh::NUM_BUFFERS];
	};

public:
	/*
	Load in a mesh using a fileName that points to an obj
	file in a directory.
	*/
	Mesh(const std::string& fileName);

	/* deconstructor */
	~Mesh();

	/*
	Draw this mesh onto the screen. Before drawing, a shader
	should be bound to the current drawing context.
	*/
	void draw();
protected:
private:
	void initMesh(const std::vector<tinyobj::shape_t> shapes);

	std::vector<Mesh::VertexArray> arrays;
	std::vector<Texture*> textures;	
};

