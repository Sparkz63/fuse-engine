#include "Camera.h"


Camera::Camera(const glm::vec3 pos, float fov, float aspect, float zNear, float zFar)
{
	perspective = glm::perspective(fov, aspect, zNear, zFar);
	position = pos;
	forwardRotation = glm::vec3(0, 0, 1);
	upRotation = glm::vec3(0, 1, 0);
}

glm::vec3 Camera::getPosition() 
{
	return position;
}

void Camera::setPosition(glm::vec3 position)
{
	this->position = position;
}

void Camera::setForwardRotation(glm::vec3 forwardRotation)
{
	this->forwardRotation = forwardRotation;
}

glm::mat4 Camera::getViewProjection() const 
{
	return perspective * glm::lookAt(position, position + forwardRotation, upRotation);
}

Camera::~Camera()
{
}
