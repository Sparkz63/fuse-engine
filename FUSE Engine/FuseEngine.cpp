#include "FuseEngine.h"
#include <iostream>

FuseEngine::FuseEngine()
{
}

FuseEngine::~FuseEngine()
{
}

/*
 Kick off main cycle. Call this method after configuration
 to run the program.
*/
void FuseEngine::run() 
{
	debugFile.open("debug.txt");

	// Run initialization, set up window
	initializeWindow();
	initialize();

	startTimers();

	// main game loop
	while (!quitApplication) {
		updateTimer();						// set deltaTime
		fetchEvents();						// get input
		update(deltaTime, eventVector);		// update game world, one frame
		render();							// render objects, one frame
		drawWindow();						// draw this frame to window
		wait();								// wait if rendered too fast
	}

	// put your toys away after you are finished
	deinitialize();
	deinitializeWindow();

	debugFile.close();
}

void FuseEngine::initializeWindow()
{
	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
	}
	else
	{
		SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

		//Create window
		window = SDL_CreateWindow(WINDOW_TITLE, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_OPENGL);

		if (window == NULL)
		{
			printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
			exit(1);
		}
		else
		{
			//Get window surface
			screenSurface = SDL_GetWindowSurface(window);

			//Get opengl context
			glContext = SDL_GL_CreateContext(window);

			//Initialize glew
			glewExperimental = GL_TRUE;
			GLenum status = glewInit();

			std::cout << "Glew Init status: " << status << std::endl;

			if (status != GLEW_OK)
			{
				printf("Glew failed to initialize");
				exit(1);
			}
		}
		glEnable(GL_CULL_FACE);
		glEnable(GL_DEPTH_TEST);
		glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
		glEnable(GL_NORMALIZE);
		glCullFace(GL_BACK);

	}

}

/*
 Populate the event queue with current SDL_Events,
 handle window events such as SDL_QUIT
*/
void FuseEngine::fetchEvents()
{
	eventVector.clear();

	SDL_Event currentEvent;

	while (SDL_PollEvent(&currentEvent) != 0)
	{
		eventVector.push_back(currentEvent);

		if (currentEvent.type == SDL_QUIT)
		{
			quitApplication = true;
		}
	}
}

/*
 draw the window onto the screen
*/
void FuseEngine::drawWindow()
{
	SDL_GL_SwapWindow(window);
	glClearColor(0.0f, 0.15f, 0.3f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

/*
 start timers
*/
void FuseEngine::startTimers()
{
	frameTimer.start();
	fpsTimer.start();
}

/*
 update deltaTime & fps
*/
void FuseEngine::updateTimer()
{
	deltaTime = frameTimer.getTime();
	frameTimer.reset();

	numFrames++;
	int frameCheckDelta = fpsTimer.getTime();
	if (frameCheckDelta > FPS_CHECK_FREQUENCY)
	{
		float seconds = frameCheckDelta / 1000.f;
		fps = numFrames / seconds;
		fpsTimer.reset();
		numFrames = 0;
	}
}

/*
 if frame was rendered too fast, wait the appropriate amount of time.
*/
void FuseEngine::wait() {
	Uint32 currentDeltaTime = frameTimer.getTime();
	if (currentDeltaTime < TARGET_TICKS_PER_FRAME)
	{
		unsigned int waitLength = TARGET_TICKS_PER_FRAME - currentDeltaTime;
		SDL_Delay(waitLength);
	}
}

/*
 free memory and close window
*/
void FuseEngine::deinitializeWindow() 
{
	//Delete openGl context
	SDL_GL_DeleteContext(glContext);

	//Destroy window
	SDL_DestroyWindow(window);

	//Quit SDL subsystems
	SDL_Quit();
}