#pragma once
#include <SDL.h>

class Timer
{
public:
	Timer();
	~Timer();
	void start();
	Uint32 pause();
	void reset();
	Uint32 getTime();

private:
	Uint32 startTime;
	Uint32 additionalTime;
	bool isPaused;
};

