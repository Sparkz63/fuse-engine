#include "Timer.h"


Timer::Timer()
{
	startTime = 0;
	additionalTime = 0;
	isPaused = false;
}

Timer::~Timer()
{
}

/*
 Start or resume the Timer.
*/
void Timer::start() 
{
	startTime = SDL_GetTicks();
	isPaused = false;
}

/*
 Pause the timer, can be resumed later.
*/
Uint32 Timer::pause() 
{
	additionalTime = getTime();
	isPaused = true;
	return getTime();
}

/*
 Get the current time since the timer started.
*/
Uint32 Timer::getTime()
{
	if (isPaused)
		return additionalTime;
	else
		return (SDL_GetTicks() - startTime) + additionalTime;
}

/*
 Reset the timer back to 0ms.
*/
void Timer::reset() 
{
	additionalTime = 0;
	startTime = SDL_GetTicks();
}
