#pragma once

#include <string>
#include <glew.h>
#include "Transform.h"
#include "Camera.h"

/*
This class represents a simple shader program.
*/
class Shader
{
public:
	/*
	Load a shader by file name. The vertex shader is expected to be
	located at filename + ".vs", the fragment shader is expected to be
	located at filename + ".fs".
	*/
	Shader(const std::string &filename);

	/*
	Bind this shader program to the current drawing context.
	*/
	void bind();

	/*
	Update this shader's transform and camera.
	*/
	void update(Transform &transform, const Camera& camera);

	/* deconstructor */
	~Shader();

private: 
	static const unsigned int NUM_SHADERS = 2;

	enum
	{
		TRANSFORM_U,

		NUM_UNIFORMS
	};

	GLuint program;
	GLuint shaders[NUM_SHADERS];
	GLuint uniforms[NUM_UNIFORMS];
};

