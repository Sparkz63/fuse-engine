#pragma once

#include <string>
#include "glm.hpp"
#include "Texture.h"
#include "Shader.h"

using std::string;

class SkyBox
{
public:
	SkyBox(string directory, string front, string back, string left, string right, string top, string bottom);
	~SkyBox();

	void render(Camera &camera);


private:
	Texture *textures[6];
	Transform *transform;
	Shader *skyShader;

	enum
	{
		POSITION_VB,
		TEXCOORD_VB,
		NORMAL_VB,

		NUM_BUFFERS
	};

	GLuint vertexArrayObject;
	GLuint vertexArrayBuffers[NUM_BUFFERS];
};

