#pragma once

#include <glm.hpp>
#include <gtx/transform.hpp>
#include <iostream>

class Transform
{
public:

	Transform(glm::vec3 &ipos = glm::vec3(0, 0, 0), glm::vec3 &irot = glm::vec3(0, 0, 0), glm::vec3 &iscale = glm::vec3(1.0f, 1.0f, 1.0f))
	{
		this->pos = ipos;
		this->rot = irot;
		this->scale = iscale;
	}

	glm::mat4 getModel();

	inline glm::vec3 getPos() { return pos; }
	inline glm::vec3 getRot() { return rot; }
	inline glm::vec3 getScale() { return scale; }

	inline void setPos(glm::vec3 pos) { this->pos = pos; }
	inline void setRot(glm::vec3 rot) { this->rot = rot; }
	inline void setScale(glm::vec3 scale) { this->scale = scale; }

	~Transform();

private:
	glm::vec3 pos;
	glm::vec3 rot;
	glm::vec3 scale;
};

