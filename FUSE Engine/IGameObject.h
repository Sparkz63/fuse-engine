#pragma once

#include <SDL.h>
#include <iostream>
#include <vector>

class IGameObject
{
public:
	virtual void initialze() = 0;
	virtual void update(Uint32 deltaTime, std::vector<SDL_Event> eventVector) = 0;
	virtual void render() = 0;
	//virtual ~IGameObject() ;
};

