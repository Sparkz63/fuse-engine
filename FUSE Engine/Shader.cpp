#include "Shader.h"
#include <iostream>
#include <fstream>

static void checkShaderError(GLuint  shader, GLuint flag, bool isProgram, const std::string &errorMessage);
static std::string loadShader(const std::string &filename);
static GLuint createShader(const std::string &text, GLenum shaderType);

Shader::Shader(const std::string &filename)
{
	// Create a program object
	program = glCreateProgram();

	// Create vertex and fragment shaders
	shaders[0] = createShader(loadShader(filename + ".vs"), GL_VERTEX_SHADER);		//Vertex shader
	shaders[1] = createShader(loadShader(filename + ".fs"), GL_FRAGMENT_SHADER);	//Fragment shader

	// Attach shaders to program
	for (unsigned int i = 0; i < NUM_SHADERS; i++)  {
		glAttachShader(program, shaders[i]);
	}

	// Bind attribute locations to program
	glBindAttribLocation(program, 0, "position");
	glBindAttribLocation(program, 1, "texCoord");
	glBindAttribLocation(program, 2, "normal");


	glLinkProgram(program);
	checkShaderError(program, GL_LINK_STATUS, true, "Error: program failed to link shaders; ");

	glValidateProgram(program);
	checkShaderError(program, GL_VALIDATE_STATUS, true, "Error: program invalid; ");

	uniforms[TRANSFORM_U] = glGetUniformLocation(program, "transform");
}

void Shader::bind() 
{
	glUseProgram(program);
}

void Shader::update(Transform &transform, const Camera& camera)
{
	glm::mat4 model = camera.getViewProjection() * transform.getModel();
	glUniformMatrix4fv(uniforms[TRANSFORM_U], 1, GL_FALSE, &model[0][0]);
}


Shader::~Shader()
{
	for (unsigned int i = 0; i < NUM_SHADERS; i++)  {
		glDetachShader(program, shaders[i]);
		glDeleteShader(shaders[i]);
	}

	glDeleteProgram(program);
}

static GLuint createShader(const std::string &text, GLenum shaderType)
{
	GLuint shader = glCreateShader(shaderType);

	if (shader == 0) {
		std::cerr << "Error: Shader creation failed!" << std::endl;
	}

	const GLchar *shaderSourceStrings[1];
	GLint shaderSourceStringLengths[1];

	shaderSourceStrings[0] = text.c_str();
	shaderSourceStringLengths[0] = text.length();

	glShaderSource(shader, 1, shaderSourceStrings, shaderSourceStringLengths);
	glCompileShader(shader);

	checkShaderError(shader, GL_COMPILE_STATUS, false, "Error: shader compilation failed; ");

	return shader;
}

static std::string loadShader(const std::string &filename)
{
	std::ifstream file;
	file.open((filename).c_str());

	std::string output;
	std::string line;

	if (file.is_open())
	{
		while (file.good())
		{
			getline(file, line);
			output.append(line + "\n");
		}
	}
	else
	{
		std::cerr << "Unable to load shader: " << filename << std::endl;
	}

	return output;
}

static void checkShaderError(GLuint  shader, GLuint flag, bool isProgram, const std::string &errorMessage)
{
	GLint success = 0;
	GLchar error[1024] = { 0 };

	if (isProgram)
		glGetProgramiv(shader, flag, &success);
	else
		glGetShaderiv(shader, flag, &success);

	if (success == GL_FALSE)
	{
		if (isProgram)
			glGetProgramInfoLog(shader, sizeof(error), NULL, error);
		else
			glGetShaderInfoLog(shader, sizeof(error), NULL, error);

		std::cerr << errorMessage << ": '" << error << "'" << std::endl;
	}
}