#pragma once

#include "FuseEngine.h"
#include "Mesh.h"
#include "Shader.h"
#include "Texture.h"
#include "Transform.h"
#include "Camera.h"
#include "Player.h"
#include "SkyBox.h"

class SampleGame : public FuseEngine
{
public:
	SampleGame();
	~SampleGame();

	Vertex *vertices;
	Mesh *mesh;
	Shader *shader;
	Transform *transform;
	Camera *camera;
	Player *player;
	SkyBox *skyBox;

private:
	long frame = 0;

	virtual void initialize();
	virtual void update(Uint32 deltaTime, std::vector<SDL_Event> eventVector);
	virtual void render();
	virtual void deinitialize();

};

