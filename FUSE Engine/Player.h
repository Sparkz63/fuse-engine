#pragma once

#include <glm.hpp>
#include <gtx/rotate_vector.hpp>
#include "IGameObject.h"

class Player : public IGameObject
{
public:
	Player(glm::vec3 position, glm::vec3 forward);

	virtual void initialze();
	virtual void update(Uint32 deltaTime, std::vector<SDL_Event> eventVector);
	virtual void render();

	glm::vec3 getPosition();
	glm::vec3 getForward();

private:
	glm::vec3 position;
	glm::vec3 forward;

	void handleEvents(std::vector<SDL_Event> eventVector);

	float moveSpeed = 20.f;

	float turnSpeed = .25f;
	int turnThreshold = 0; // mouse look threshold in pixels

	glm::vec2 motion;
	float yawRotation = 0;
	float pitchRotation = 0;

	enum controls
	{
		MOVE_FORWARD,
		MOVE_BACKWARD,
		MOVE_LEFT,
		MOVE_RIGHT,
		MOVE_UP,
		MOVE_DOWN,

		NUM_CONTROLS
	};
	bool controller[NUM_CONTROLS];
};

